package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;



public class MyLeadsPage extends Annotations {
	
	public CreateLeadPage clickCreateLead() {
	WebElement eleCLead = locateElement("LinkText", "CReate Lead");
	click(eleCLead);
	return new CreateLeadPage();

}
}