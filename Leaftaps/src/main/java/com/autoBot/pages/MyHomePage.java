package com.autoBot.pages;


import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class MyHomePage extends Annotations {
	
	public MyLeadsPage clickLeads() {
		WebElement eleLeads=locateElement("link","Leads");
		click(eleLeads);
		return new MyLeadsPage();
	}

}