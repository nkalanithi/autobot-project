package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations {
	
	
	
	public CreateLeadPage enterCompanyName(String data1) {
		WebElement elecname = locateElement("id", "createLeadForm_companyName");
		clearAndType(elecname, data1);
		return this;
	}
	
	public CreateLeadPage enterFirstName(String data2) {
		WebElement elefname = locateElement("id", "createLeadForm_firstName");
		clearAndType(elefname, data2);
		return this;
	}
	
	
	public CreateLeadPage enterLastName(String data3) {
		WebElement elelname = locateElement("id", "createLeadForm_lastName");
		clearAndType(elelname, data3);
		return this;
		
	}
	
	public ViewLeadsPage clickCreateLeadButton() {
		WebElement eleclkcreatlead = locateElement("class", "smallSubmit");
		click(eleclkcreatlead);
		return new ViewLeadsPage();
	}
}
